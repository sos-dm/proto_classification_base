using UnityEngine;
using UnityEngine.SceneManagement;

// <summary> The ChangeScene class is responsible for managing scene transitions and configuring game settings such as flash and sound effects. </summary>
public class ChangeScene : MonoBehaviour {
    /// <summary> Load the scene and update the userPreference which sends to the TCP server when selected. </summary>
    /// <param name="SceneToLoad"></param>
    public void LoadScene(string SceneToLoad)
    {
        SceneManager.LoadScene(SceneToLoad);
    }

}