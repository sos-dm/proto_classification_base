using System;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// ShipBase class to manage the ship image library.
/// </summary>
public class ShipBase : MonoBehaviour
{
    /// <summary> List to hold all the ships image file names contained in the Resources Asset folder</summary>
    private static List<string> shipImageNames;

    private string nameCurrShip = "";

    public VisualTimer timer;

    public ProgressBar progressBar;

    /// <summary> Start method is called before the first frame update </summary>
    void Start () 
    {
        LoadingShipBase();
    
        UpdatingImage();
    }

    /// <summary>
    /// Load the Ship image base into a list.
    /// </summary>
    public static void LoadingShipBase() 
    {
        // Balanced image base of blurred, black and white and origin ship images.

        shipImageNames = new List<string>();

        var listShip = Resources.Load<TextAsset>("classementNavireOrigine");
        var lines = listShip.text.Split('\n');

        var nbBLR = 0;
        var nbORI = 0;
        var nbBW = 0;

        for(int i=0; i<lines.Length; i++)
        {
            if(lines[i].Length > 1) {

                var data = lines[i].Split(',');

                // var rand = UnityEngine.Random.Range(0, 3);
                var dataEXT = data[0];
                // 0 = Originale
                // 1 = B&W
                // 2 = Blurred

                var rand = 0;

                bool flagFullMode = false;
                do
                {
                    rand = UnityEngine.Random.Range(0, 3);
                    switch (rand) {
                    case 0:
                        if (nbORI == 69){
                            flagFullMode = true;
                        } else {
                            flagFullMode = false;
                        }
                        break;  
                    case 1:
                        if (nbBW == 69){
                            flagFullMode = true;
                        } else {
                            flagFullMode = false;
                        }
                        break;  
                    case 2:
                        if (nbBLR == 69){
                            flagFullMode = true;
                        } else {
                            flagFullMode = false;
                        }
                        break;  
                }
                } while (flagFullMode);

                switch (rand) {
                    case 0:
                        nbORI++;
                        break;
                    case 1:
                        dataEXT = "B&W_"+dataEXT;
                        nbBW++;
                        break;
                    case 2:
                        dataEXT = "BLR_RED_"+dataEXT;
                        nbBLR++;
                        break;
                }

                var idx = dataEXT.LastIndexOf('.');

                if(idx != -1)
                {
                    dataEXT = dataEXT.Substring(0, idx);
                }

                shipImageNames.Add(dataEXT);
            }
            
        } 
        Debug.Log("ETAT DE LA BASE - Originale : " + nbORI + " // B&W : " + nbBW + " // BLR : " + nbBLR);
    }

    /// <summary>
    /// Return a random image name from the Ship image base and delete it from the ShipBase
    /// </summary>
    public static string GetRandomImage() 
    {
        var nbShip = shipImageNames.Count;

        if (nbShip == 0) {
            // Executing the "Back" button actions
            GameObject button = GameObject.Find("Back");
            var eventSystem = EventSystem.current;
            ExecuteEvents.Execute(button.gameObject, new BaseEventData(eventSystem), ExecuteEvents.submitHandler);
            return null;
        }

        var rand = UnityEngine.Random.Range(0, nbShip); 

        var shipName = shipImageNames[rand];

        shipImageNames.Remove(shipName);

        return shipName;
    }

    /// <summary>
    /// ValidateShip is giving feedback corresponding to the selected type answered
    /// </summary>
    public void ValidateShip(string typeInput)
    {
        timer.setIsRunning(false);

        var name = transform.GetComponent<Image>().sprite.name;
        var csvFile = Resources.Load<TextAsset>("classementNavireComplet");
        var lines = csvFile.text.Split('\n');

        for(int i=0; i<lines.Length; i++)
        {
            var data = lines[i].Split(',');
            var idx = data[0].LastIndexOf('.');
            var dataEXT = data[0];

            if(idx != -1)
            {
                dataEXT = data[0].Substring(0, idx);
            }

            if(dataEXT == name) 
            {
                if(Int32.Parse(data[1]) == Int32.Parse(typeInput))
                {
                    Log.AddLog(nameCurrShip, Int32.Parse(data[1]), Int32.Parse(typeInput), true);
                } else {
                    Log.AddLog(nameCurrShip, Int32.Parse(data[1]), Int32.Parse(typeInput), false);
                }
                UpdatingImage();
                return;
            } 
        }  
        Debug.LogError("Ship not found in DataBase");
    }  

    

    /// <summary>
    /// UpdatingImage is fetching a new image to display and resetting the VisualTimer to 5
    /// </summary>
    public void UpdatingImage() {

        Image image = GetComponent<Image>();
        nameCurrShip = GetRandomImage();
        if (image == null || nameCurrShip == null){
            Debug.LogError("IMAGE NULL");
        } else {
            progressBar.incrNbImg();
            Texture2D tex = Resources.Load(nameCurrShip) as Texture2D;
            Sprite newSprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
            image.sprite = newSprite;
            image.sprite.name = nameCurrShip;
            timer.setTimeLeft(5f);
            timer.setIsRunning(true);
        }

    }

    /// <summary>
    /// getCurrShipName is returning the nameCurrShip variable
    /// </summary>
    public string getCurrShipName(){
        return nameCurrShip;
    }
}