using UnityEngine;
using System.IO;

/// <summary>
/// Log class that handles the logs.
/// </summary>
public class Log : MonoBehaviour
{
    /// <summary>
    /// The username variable, storing the username.
    /// </summary>
    private static string username;

    /// <summary>
    /// The path variable, storing the path.
    /// </summary>
    private string path = Directory.GetCurrentDirectory();

    /// <summary>
    /// The ficName variable, storing the file name.
    /// </summary>
    private static string ficName;

    /// <summary>
    /// The timeInit variable, storing the initial time.
    /// </summary>
    private static long timeInit;

    /// <summary>
    /// The isLog variable, storing whether the log is active.
    /// </summary>
    private static bool isLog = false;

    /// <summary>
    /// The tracked variable, storing the number of tracked image.
    /// </summary>
    private static int tracked = 0;

    /// <summary>
    /// The failed variable, storing the number of failed image.
    /// </summary>
    private static int failed = 0;

    /// <summary>
    /// The misse variable, storing the number of missed image.
    /// </summary>
    private static int missed = 0;

    /// <summary>
    /// Create a new log file, given the scenario and entering the initial time with the details inside of the log file.
    /// </summary>
    public void createNewFic() 
    {
        timeInit = System.DateTimeOffset.UtcNow.ToUnixTimeSeconds();
        ficName = "log_" + timeInit.ToString() + "_Classement_" + username + ".csv";

        string userFolderPath = Path.Combine(path, "Logs", username);
        if (!Directory.Exists(userFolderPath))
        {
            Directory.CreateDirectory(userFolderPath);
        }

        string logFilePath = Path.Combine(userFolderPath, ficName);

        File.WriteAllText(logFilePath, "timestamp, temps en seconde, idImage, typeImg, typeInput, nbMissed, nbTracked, nbFailed, result\n");

        isLog = true;

        tracked = 0;
        failed = 0;
        missed = 0;
    }

    /// <summary>
    /// Add a log to the log file. 
    /// 
    /// </summary>
    /// <param name="idImage"></param>
    /// <param name="typeImg"></param>
    /// <param name="typeInput"></param>
    /// <param name="result"></param>
    public static void AddLog(string idImage, int typeImg, int typeInput, bool result) 
    {
        if (isLog) {

            if(typeInput == -1)
            {
                missed++;
            } else if (result)
            {
                tracked++;
            }
            else 
            {
                failed++;
            }

            

            long timeCurrent = System.DateTimeOffset.UtcNow.ToUnixTimeSeconds() - timeInit;
            string userFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "Logs", username);
            string logFilePath = Path.Combine(userFolderPath, ficName);

            string logMessage = System.DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString() + ", " +
                                timeCurrent.ToString() + ", " +
                                idImage + ", " +
                                typeImg + ", " +
                                typeInput + ", " +
                                missed + ", " +
                                tracked + ", " +
                                failed + ", " +
                                result + "\n";

            File.AppendAllText(logFilePath, logMessage);
        }
    }

    /// <summary>
    /// End the log file.
    /// </summary>
    public void endFic() 
    {
        string userFolderPath = Path.Combine(Directory.GetCurrentDirectory(), "Logs", username);
        string logFilePath = Path.Combine(userFolderPath, ficName);

        File.AppendAllText(logFilePath, "Fin simulation" + System.DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString() + "\n");
    }

    /// <summary>
    /// Set the username.
    /// </summary>
    /// <param name="u"></param>
    public static void setUsername(string u) 
    {  
        username = u;
    }
}
