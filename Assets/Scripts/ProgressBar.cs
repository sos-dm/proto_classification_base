using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    /// <summary> Image to hold the Instance of the bar visual </summary>
    public Image progressBar;

    /// <summary> Float to hold the max time value </summary>
    public int maxImage = 207;

    private int currImg;
    // Start is called before the first frame update
    void Start()
    {
        progressBar = GetComponent<Image>();
        currImg = 0;
    }

    // Update is called once per frame
    void Update()
    {
        progressBar.fillAmount = (float)currImg / (float)maxImage;
    }

    public void incrNbImg(){
        currImg++;
    }
}

