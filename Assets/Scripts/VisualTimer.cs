using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// VisualTimer class is used to display a visual bar timer of 5 seconds. This timer is refreshed at the update of the picture.
/// </summary>
public class VisualTimer : MonoBehaviour
{
    /// <summary> Image to hold the Instance of the bar visual </summary>
    public Image timeBar;

    /// <summary> Float to hold the max time value </summary>
    public float maxTime = 5f;

    /// <summary> Float to hold the current remaining time value </summary>
    private float timeLeft;

    /// <summary> ShipBase to hold the script of the classification GameObject instance </summary>
    public ShipBase shipScript;

    private bool isTimeRunning;

    /// <summary> Start is called before the first frame update </summary>
    void Start()
    {
        timeBar = GetComponent<Image>();
        timeLeft = maxTime;
        isTimeRunning = true;
    }

    /// <summary> Update is called once per frame </summary>
    void Update()
    {
        if(isTimeRunning){
            if(timeLeft > 0) {
                timeLeft -= Time.deltaTime;
                timeBar.fillAmount = timeLeft / maxTime;
            } else {
                //If the time is up, the image is updated
                shipScript.UpdatingImage();
                Log.AddLog(shipScript.getCurrShipName(), -1, -1, false);
            }
        }
    }

    /// <summary>
    /// The setTimeLeft method is a setter for the timeLeft variable
    /// </summary>
    /// <param name="time"></param>
    public void setTimeLeft(float time){
        timeLeft = time;
    }

    public void setIsRunning(bool isRunning){
        isTimeRunning = isRunning;
    }
}
