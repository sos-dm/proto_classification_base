# proto_classification_base


## Installation

```
git clone https://gitlab.imt-atlantique.fr/sos-dm/proto_classification_base.git
```

Récupérer l'archive contenant les dossiers Resources (contenant la base d'images de bateau et deux fichiers CSV) ainsi que Navires_Consignes.

Installer Unity Hub : https://unity.com/fr/download

Installer la version d'Unity 2020.3.23f1

Ouvrir le projet avec Unity Hub 

Dezipper l'archive et mettre le contenu du dossier Resources dans le dossier Assets/Resources dans l'arborescence du projet sur Unity, de même, mettre le contenu de Navires_Consignes dans le dossier Assets/SFX.

Lancer la scène 'MainMenu'